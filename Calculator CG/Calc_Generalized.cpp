#include <iostream>
#include <math.h>
#include <cmath>
using namespace std;

#define sign(x) ((x>0)?1:((x<0)?-1:0))

int main()
{
int x,y,x1,x2,y1,y2,dx,dy,ex,s1,s2;
static int e;

int xr[15];
int yr[15];

//Step 1
cout<<"Enter x1 and y1 "<<endl;
cin>>x1>>y1;
cout<<"Enter x2 and y2 "<<endl;
cin>>x2>>y2;

//Step 2
dx = abs(x2-x1);
dy = abs(y2-y1);

cout << "---------------" << endl;
cout << "dx = " << dx << endl;
cout << "dy = " << dy << endl;

//Step 3
x=x1;
y=y1;
cout << "---------------" << endl;
cout << "x = "<< x <<"|  y = "<<y<< endl;
cout << "---------------" << endl;
//Step 4
s1 = sign(x2-x1);
s2 = sign(y2-y1);

cout << "s1 = sign(x2-x1)" << endl;
cout << "s1 = " << "sign( " << x2 << "-" <<  x1 << " )"<< endl;
cout << "s1 = " << s1 << endl;
cout << "---------------" << endl;
cout << "s2 = sign(y2-y1)" << endl;
cout << "s2 = " << "sign( " << y2 << "-" <<  y1 << " )"<< endl;
cout << "s2 = " << s2 << endl;
cout << "---------------" << endl;

//step 5
if(dy>dx){
    int t = dx;
    dx = dy;
    dy = t;

    ex = 1;
}
else {
    ex = 0;
}

//step 6
cout << "e=(2*dy)-dx" << endl;
cout << "e=" << 2*dy << "-" << dx << endl;
e=(2*dy)-dx;
cout << "e main =" << e << endl;
cout << "---------------" << endl;

//Step 7
cout << "\n<-- Calculations -->\n" << endl;
for(int i=1;i<=dx;i++){
         cout << "\n(i = " << i << " )" << endl;
    while(e>=0){

        if(ex == 1){
            cout << "x = x + s1"<< endl;
            cout << "x = "<<x << "+" << s1 << endl;
            x=x+s1;
            cout << "x = " << x<< endl;
        }
        else{
            cout << "y = y + s2"<< endl;
            cout << "y = "<<y << "+" << s2 << endl;
            y=y+s2;
            cout << "y = " << y<< endl;
        }
    cout << "\ne=e-2*dx" << endl;
    cout << "e=" << e << "-(2*" << dx << ")" << endl;
    cout << "e=" << e << "-" << 2*dx << endl;
    e=e-2*dx;
    cout << "e(1)-->"<< e << endl;
}
    if(ex==1){
    cout << "y = y + s2"<< endl;
    cout << "y = "<<y << "+" << s2 << endl;
    y=y+s2;
    cout << "y = " << y<< endl;
    }
    else{
    cout << "x = x + s1"<< endl;
    cout << "x = "<<x << "+" << s1 << endl;
    x=x+s1;
    cout << "x = " << x<< endl;
    }
    cout << "\ne=e+2*dy" << endl;
    cout << "e=" << e << "+(2*" << dy << ")" << endl;
    cout << "e=" << e << "+" << 2*dy << endl;
    e=e+2*dy;
    cout << "e(2)-->"<< e << endl;
    cout << "---------------------> (x , y) = " << "(" << x << " , " << y << ")"<< endl;
    xr[i]=x;
    yr[i]=y;
}
cout << "\n\n----- result -----\n" << endl;
for(int i=1;i<=dx;i++){
   cout << "--------> (x , y) = " << "(" << xr[i] << " , " << yr[i] << ")"<< endl;
}
return 0;
}
