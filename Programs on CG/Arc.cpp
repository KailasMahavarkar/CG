#include <iostream>
#include <graphics.h>
using namespace std;

int main(){
    initwindow(600,600);
    int x,y,r,d;
    cout << "Note : Max Possible radius " << 600/2 << endl;
    cout << "Enter radius "<<endl;
    cin>>r;
    x=0;
    y=r;
    d= 3-2*r;
    do{
        putpixel(r+x,r+y,5);
        if(d<0){
            d=d+4*x+6;
        }
        else{
            d=d+4*(x-y)+10;
            y=y-1;
        }
        x=x+1;
        delay(10);
    }while(x<y);

    getch();
    closegraph();
    return 0;
}
