#include <iostream>
#include <conio.h>
#include <graphics.h>
using namespace std;

int main()
{
    int n,x,y,x1,y1,x2,y2,dx,dy,length;
    initwindow(1000,1000);
    cout << "Enter x1 & y1" << endl;
    cin >> x1 >> y1;
    cout << "Enter x2 & y2" << endl;
    cin >> x2 >> y2;

    dx = abs(x2-x1);
    dy = abs(y2-y1);

    if(dx>dy){
       length =dx;
    }
    else{
       length =dy;
    }

    dx = (x2-x1)/length;
    dy = (y2-y1)/length;

    x=x1+0.5;
    y=y1+0.5;

    for(int i=1;i<=length;i++){
    putpixel(x,y,5);
    x = x + dx;
    y = y + dy;
    delay(10);
    }

    getch();
    closegraph();
    return 0;
}
