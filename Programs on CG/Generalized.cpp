#include <iostream>
#include <graphics.h>
#include <math.h>
using namespace std;

#define sign(x) ((x>0)?1:((x<0)?-1:0))

int main()
{
    int x,y,x1,y1,x2,y2,dx,dy;
    int e,ex,temp,s1,s2;
    initwindow(1000,1000);
    cout << "Enter x1 & y1" << endl;
    cin >> x1 >> y1;
    cout << "Enter x2 & y2" << endl;
    cin >> x2 >> y2;

    dx = abs(x2-x1);
    dy = abs(y2-y1);

    x = x1;
    y = y1;

    s1 = sign(x2-x1);
    s2 = sign(y2-y1);

    if(dy>dx){
        temp = dy;
        dy = dx;
        dx = temp;
        ex=1;
    }
    else{
        ex=0;
    }

    e = (2*dy)-dx;

for(int i=1;i<=dx;i++){
    while(e>=0){

        if(ex==1){
            x=x+s1;
        }
        else{
            y=y+s2;
        }
        e =e-2*dx;
    }

    if(ex==1){
        y=y+s2;
    }
    else{
        x=x+s1;
    }
    e =e+2*dy;
	delay(10);
    putpixel(x,y,5);
}
    getch();
    closegraph();
    return 0;
}
